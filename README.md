Travel Rule Protocol (TRP)
=====================================

[The Travel Rule Protocol (TRP)](https://www.openvasp.org/trp) is a mature, complete API for compliance with FATF Travel Rule recommendations for virtual assets, which is ready and in use today.

This repository also contains the a collection of protocol [extensions](/extensions). To suggest a new extension, please join our working group through the [website](https://www.openvasp.org/trp).


# Founding Organizations

The Travel Rule Protocol working group was founded by and the first
draft version of the TRP protocol was created by Standard Chartered, ING
and BitGo. Through welcoming open industry collaboration the working
group has since grown to encompass a large number of leading
organizations from across the global Virtual Asset Industry and
continues to welcome further organizations to join.

The working group is currently chaired by Andrew Davidson.

You can find a list of contributors [here](https://gitlab.com/OpenVASP/travel-rule-protocol/-/graphs/master).

TRP is a part of the [OpenVASP Association](https://openvasp.org/).
